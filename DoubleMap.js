base_url = "http://mbus.doublemap.com/map/v2/";

function GetStopETA(stop_id, route_id, cb) {

    // Create a request variable and assign a new XMLHttpRequest object to it.
    var request = new XMLHttpRequest();

    // Open a new connection, using the GET request on the URL endpoint
    request.open('GET', base_url + "eta?stop=" + stop_id, true);

    request.onload = function () {
        // Begin accessing JSON data here
        var data = JSON.parse(this.response);

        if (request.status >= 200 && request.status < 400) {
            var stop_data = data["etas"][stop_id]["etas"];
            var stop_eta = stop_data.find(
                function (bus) {
                    return bus["route"] == route_id;
                }
            )["avg"];

            cb(stop_eta);

        }
        else {
            console.log('error');
        }
    }

    request.timeout = 2000;

    // Send request
    request.send();
}